﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;
    public static bool isElChapo;
    public static bool isKnight;
    public static bool isGuy;
    public static bool gameRestart;
    public static bool menu;


    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private GameObject elChapo;
    private GameObject knight;
    private GameObject guy;
    private GameObject startOver;

	void Awake  () {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
	
	}

    public void ElChapo()
    {
        isElChapo = true;
        elChapo.SetActive(false);

    }

    public void Knight()
    {
        isKnight = true;
        Player.animator.SetTrigger("knight");
        knight.SetActive(false);
    }

    public void Guy()
    {
        isGuy = true;
        Player.animator.SetTrigger("guy");
        guy.SetActive(false);
    }

    void Start()
    {
        elChapo = GameObject.Find("El Chapo");
        knight = GameObject.Find("Knight");
        guy = GameObject.Find("Guy");
        IntitalizeGame();
    }

    private void IntitalizeGame()
    {
        settingUpGame = true;
        levelImage = GameObject.Find("Level Image");
        elChapo = GameObject.Find("El Chapo");
        knight = GameObject.Find("Knight");
        guy = GameObject.Find("Guy");
        startOver = GameObject.Find("Game Restart");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
        levelText.text = "Day " + currentLevel;
        levelImage.SetActive(true);
        enemies.Clear();
        boardController.SetupLevel(currentLevel);
            Invoke("DisableLevelImage", secondsUntilLevelStart);
    }

    private void DisableLevelImage()
    {
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;

        if (isKnight)
        {
            Player.animator.SetTrigger("knight");
        }

        if (isGuy)
        {
            Player.animator.SetTrigger("guy");
        }

        if (currentLevel != 1)
        {
            levelImage.SetActive(false);
        }

      
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        IntitalizeGame();
    }
	
	void Update () {
        if(isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
	}

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach(Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }


        areEnemiesMoving = false;
        isPlayerTurn = true;
    }
    
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    public void StartOver()
    {
        if(currentLevel == 0)
        {
            gameRestart = true;
        }
        startOver.SetActive(false);
    }

    public void GameOver()
    {
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        levelText.text = "You starved after " + currentLevel + " days...";
        levelImage.SetActive(true);
        enabled = false;
    }
}
